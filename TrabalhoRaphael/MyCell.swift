//
//  MyCell.swift
//  TrabalhoRaphael
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var ImagePersonagem: UIImageView!
    @IBOutlet weak var NomeAutor: UILabel!
    @IBOutlet weak var NomePersonagem: UILabel!
    

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
